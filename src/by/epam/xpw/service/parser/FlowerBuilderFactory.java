package by.epam.xpw.service.parser;

import by.epam.xpw.service.dom.FlowersDOMBuilder;
import by.epam.xpw.service.sax.FlowersSAXBuilder;
import by.epam.xpw.service.stax.FlowersStAXBuilder;

public class FlowerBuilderFactory {
	
	private enum TypeParser {
		SAX, STAX, DOM
	}
	
	public AbstractFlowersBuilder createFlowerBuilder(String typeParser) {
		TypeParser type = TypeParser.valueOf(typeParser.toUpperCase());
		switch (type) {
		case DOM:
			return new FlowersDOMBuilder();
		case STAX:
			return new FlowersStAXBuilder();
		case SAX:
			return new FlowersSAXBuilder();
		default:
			throw new EnumConstantNotPresentException(type.getDeclaringClass(), type.name());
		}
	}
	
}
