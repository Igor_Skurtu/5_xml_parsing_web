package by.epam.xpw.service.parser;

import java.util.ArrayList;
import java.util.List;

import by.epam.xpw.entity.FlowerType;

public abstract class AbstractFlowersBuilder {

	protected List<FlowerType> flowers;

	public AbstractFlowersBuilder() {
		flowers = new ArrayList<FlowerType>();
	}

	public AbstractFlowersBuilder(List<FlowerType> flowers) {
		this.flowers = flowers;
	}

	public List<FlowerType> getFlowers() {
		return flowers;
	}

	abstract public void buildListFlowers(String fileName);
}
