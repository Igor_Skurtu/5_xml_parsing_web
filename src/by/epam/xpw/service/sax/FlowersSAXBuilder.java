package by.epam.xpw.service.sax;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import by.epam.xpw.entity.FlowerType;
import by.epam.xpw.service.parser.AbstractFlowersBuilder;

public class FlowersSAXBuilder extends AbstractFlowersBuilder{
	
	private static final Logger LOGGER = LogManager.getLogger(FlowersSAXBuilder.class);
	
	private List<FlowerType> flowers;
	private FlowerHandler fh;
	private XMLReader reader;

	public FlowersSAXBuilder() {
		fh = new FlowerHandler();
		try {
			reader = XMLReaderFactory.createXMLReader();
			reader.setContentHandler(fh);
		} catch (SAXException e) {
			LOGGER.error("SAX parser error: " + e);
		}
	}
	
	public FlowersSAXBuilder(List<FlowerType> flowers) {
		super(flowers);
	}

	public List<FlowerType> getFlowers() {
		return flowers;
	}
	
	@Override
	public void buildListFlowers(String fileName) {
		try {
			reader.parse(new InputSource(new FileInputStream(fileName)));
		} catch (SAXException e) {
			LOGGER.error("SAX parser error: " + e);
		} catch (IOException e) {
			LOGGER.error("I/O stream error: " + e);
		}
		flowers = fh.getFlowers();
	}

}
