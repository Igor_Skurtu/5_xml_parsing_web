package by.epam.xpw.service.sax;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import by.epam.xpw.entity.FlowerForRent;
import by.epam.xpw.entity.FlowerForSale;
import by.epam.xpw.entity.FlowerType;
import by.epam.xpw.entity.FlowerTypeEnum;
import by.epam.xpw.entity.MultiplyingType;
import by.epam.xpw.entity.SoilType;

public class FlowerHandler extends DefaultHandler{
	
	private List<FlowerType> flowers;
	private FlowerType current = null;
	private FlowerTypeEnum currentEnum = null;
	private EnumSet<FlowerTypeEnum> withText;

	public FlowerHandler() {
		flowers = new ArrayList<FlowerType>();
		withText = EnumSet.range(FlowerTypeEnum.NAME, FlowerTypeEnum.PRICEUSDDAY);
	}

	public List<FlowerType> getFlowers() {
		return flowers;
	}

	public void startElement(String uri, String localName, String qName,
			Attributes attrs) {
		if ("flower-for-sale".equals(localName) || "flower-for-rent".equals(localName)) {
						
			if("flower-for-sale".equals(localName)) {
				current = new FlowerForSale();
				current.setFlowerId(attrs.getValue(0));
				current.setForSale(true);
				String tempString = (attrs.getValue(2) == null) ? "good" : attrs.getValue(2);
				((FlowerForSale)current).setExpirationStatus(tempString);
			}
			
			if("flower-for-rent".equals(localName)) {
				current = new FlowerForRent();
				current.setFlowerId(attrs.getValue(0));
				current.setForSale(false);
				((FlowerForRent)current).setRentalTimeDays(Integer.parseInt(attrs.getValue(1)));
			}

		} else {
			FlowerTypeEnum temp = FlowerTypeEnum.valueOf(localName.replaceAll("-", "").toUpperCase());
			if (withText.contains(temp)) {
				currentEnum = temp;
			}
		}
	}

	public void endElement(String uri, String localName, String qName) {
		if ("flower-for-sale".equals(localName) || "flower-for-rent".equals(localName)) {
			flowers.add((FlowerType)current);
		}
	}

	public void characters(char[] ch, int start, int length) {
		String s = new String(ch, start, length).trim();
		if (currentEnum != null) {
			switch (currentEnum) {
			case NAME:
				current.setName(s);
				break;
			case SOIL:
				current.setSoil(SoilType.fromValue(s));
				break;
			case ORIGIN:
				current.setOrigin(s);
				break;
			case STEMCOLOR:
				current.getVisualParameters().setStemColor(s);
				break;
			case LEAFCOLOR:
				current.getVisualParameters().setLeafColor(s);
				break;
			case AVERAGESIZECM:
				current.getVisualParameters().setAverageSizeCm(Integer.parseInt(s));
				break;
			case TEMPERATUREC:
				current.getGrowingTips().setTemperatureC(Short.parseShort(s));
				break;	
			case LIGHTING:
				current.getGrowingTips().setLighting(s);
				break;
			case WEEKWATERINGML:
				current.getGrowingTips().setWeekWateringMl(Integer.parseInt(s));
				break;
			case MULTIPLYING:
				current.setMultiplying(MultiplyingType.fromValue(s));
				break;
			case PRICEUSD:
				if(!s.isEmpty()) {
					((FlowerForSale)current).setPriceUsd(Double.parseDouble(s));
				} else {((FlowerForSale)current).setPriceUsd(4.99);}
				break;
			case PRICEUSDDAY:
				if(!s.isEmpty()) {
					((FlowerForRent)current).setPriceUsdDay(Double.parseDouble(s));
				} else {((FlowerForRent)current).setPriceUsdDay(4.99);}
				
				break;
			default:
				throw new EnumConstantNotPresentException(
						currentEnum.getDeclaringClass(), currentEnum.name());
			}
		}
		currentEnum = null;
	}
}
