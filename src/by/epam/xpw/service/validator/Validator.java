package by.epam.xpw.service.validator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import by.epam.xpw.service.sax.FlowerErrorHandler;

public class Validator {
	
	private static final Logger LOGGER = LogManager.getLogger(Validator.class);
	
	public static boolean validate(String xmlPath, String xsdPath) {
		Schema schema = null;
		String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
		SchemaFactory factory = SchemaFactory.newInstance(language);
		try {	
			schema = factory.newSchema(new File(xsdPath));
			SAXParserFactory spf = SAXParserFactory.newInstance();
			spf.setNamespaceAware(true);
			spf.setSchema(schema);
			SAXParser parser = spf.newSAXParser();
			parser.parse(new FileInputStream(xmlPath), new FlowerErrorHandler());
			return true;
		} catch (ParserConfigurationException e) {
			LOGGER.error(xmlPath + " config error: " + e.getMessage());
			return false;
		} catch (SAXException e) {
			LOGGER.error(xmlPath + " SAX error: " + e.getMessage());
			return false;
		} catch (IOException e) {
			LOGGER.error("I/O error: " + e.getMessage());
			return false;
		}
	}
}
