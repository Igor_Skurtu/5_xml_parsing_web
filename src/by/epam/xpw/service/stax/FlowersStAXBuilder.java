package by.epam.xpw.service.stax;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epam.xpw.entity.FlowerForRent;
import by.epam.xpw.entity.FlowerForSale;
import by.epam.xpw.entity.FlowerType;
import by.epam.xpw.entity.FlowerTypeEnum;
import by.epam.xpw.entity.GrowingTips;
import by.epam.xpw.entity.MultiplyingType;
import by.epam.xpw.entity.SoilType;
import by.epam.xpw.entity.VisualParameters;
import by.epam.xpw.service.parser.AbstractFlowersBuilder;

public class FlowersStAXBuilder extends AbstractFlowersBuilder {

	private static final Logger LOGGER = LogManager.getLogger(FlowersStAXBuilder.class);
	
	private List<FlowerType> flowers = new ArrayList<>();
	private XMLInputFactory inputFactory;

	public FlowersStAXBuilder() {

		inputFactory = XMLInputFactory.newInstance();
	}

	public FlowersStAXBuilder(List<FlowerType> flowers) {
		super(flowers);
	}
	
	public List<FlowerType> getFlowers() {
		return flowers;
	}
	
	@Override
	public void buildListFlowers(String fileName) {
		FileInputStream inputStream = null;
		XMLStreamReader reader = null;
		String name;
		try {
			inputStream = new FileInputStream(new File(fileName));
			reader = inputFactory.createXMLStreamReader(inputStream);
			while (reader.hasNext()) {
				int type = reader.next();
				if (type == XMLStreamConstants.START_ELEMENT) {
					name = reader.getLocalName();
					
					if (name == FlowerTypeEnum.FLOWERFORSALE.getValue() ) {
						FlowerType fs = buildFlowerForSale(reader);
						flowers.add(fs);
					}
				
					if (name == FlowerTypeEnum.FLOWERFORRENT.getValue() ) {
						FlowerType fr = buildFlowerForRent(reader);
						flowers.add(fr);
					}
				}
			}
		} catch (XMLStreamException ex) {
			LOGGER.error("StAX parsing error! " + ex.getMessage());
		} catch (FileNotFoundException ex) {
			LOGGER.error("File " + fileName + " not found! " + ex);
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				LOGGER.error("Impossible close file " + fileName + " : " + e);
			}
		}
	}

	
	private FlowerType buildFlowerForSale(XMLStreamReader reader) throws XMLStreamException {
		
		FlowerForSale fs = new FlowerForSale();
		fs.setFlowerId(reader.getAttributeValue(null, FlowerTypeEnum.FLOWERID.getValue()));
		fs.setForSale(Boolean.parseBoolean(reader.getAttributeValue(null, FlowerTypeEnum.FORSALE.getValue())) );
		
		String name = reader.getAttributeValue(null, FlowerTypeEnum.EXPIRATIONSTATUS.getValue());
		if(name != null) {
			fs.setExpirationStatus(name);
		} else {fs.setExpirationStatus("good");}
				
		
		while (reader.hasNext()) {
			int type = reader.next();
			switch (type) {
			case XMLStreamConstants.START_ELEMENT:
				name = reader.getLocalName();
				switch (FlowerTypeEnum.valueOf(name.replaceAll("-", "").toUpperCase())) {

				case NAME:
					fs.setName(getXMLText(reader));
					break;
				case SOIL:
					name = getXMLText(reader);
					fs.setSoil(SoilType.fromValue(name));
					break;
				case ORIGIN:
					fs.setOrigin(getXMLText(reader));
					break;
				case VISUALPARAMETERS:
					fs.setVisualParameters(getXMLVisualParameters(reader));
					break;
				case GROWINGTIPS:
					fs.setGrowingTips(getXMLGrowingTips(reader));
					break;
				case MULTIPLYING:
					fs.setMultiplying(MultiplyingType.fromValue(getXMLText(reader)));
					break;
				case PRICEUSD:
					String temp = getXMLText(reader);
					if(!temp.isEmpty()) {
						fs.setPriceUsd(Double.parseDouble(temp));
					} else {fs.setPriceUsd(4.99);}
					
				default:
					break;
				}
				break;
			case XMLStreamConstants.END_ELEMENT:
				name = reader.getLocalName().replaceAll("-", "").toUpperCase();
				
				if (FlowerTypeEnum.valueOf(name) == FlowerTypeEnum.FLOWERFORSALE) {
					return fs;
				}
				break;
			}
		}
		throw new XMLStreamException("Unknown element in tag flower-for-sale");
	}

	
	private FlowerType buildFlowerForRent(XMLStreamReader reader) throws XMLStreamException {
		
		FlowerForRent fr = new FlowerForRent();
		fr.setFlowerId(reader.getAttributeValue(null, FlowerTypeEnum.FLOWERID.getValue()));
		fr.setForSale(false);
		fr.setRentalTimeDays(Integer.parseInt(reader.getAttributeValue(null, FlowerTypeEnum.RENTALTIMEDAYS.getValue())));
				
		String name;
		while (reader.hasNext()) {
			int type = reader.next();
			switch (type) {
			case XMLStreamConstants.START_ELEMENT:
				name = reader.getLocalName();
				switch (FlowerTypeEnum.valueOf(name.replaceAll("-", "").toUpperCase())) {

				case NAME:
					fr.setName(getXMLText(reader));
					break;
				case SOIL:
					name = getXMLText(reader);
					fr.setSoil(SoilType.fromValue(name));
					break;
				case ORIGIN:
					fr.setOrigin(getXMLText(reader));
					break;
				case VISUALPARAMETERS:
					fr.setVisualParameters(getXMLVisualParameters(reader));
					break;
				case GROWINGTIPS:
					fr.setGrowingTips(getXMLGrowingTips(reader));
					break;
				case MULTIPLYING:
					fr.setMultiplying(MultiplyingType.fromValue(getXMLText(reader)));
					break;
				case PRICEUSDDAY:
					String temp = getXMLText(reader);
					if(!temp.isEmpty()) {
						fr.setPriceUsdDay(Double.parseDouble(temp));
					} else {fr.setPriceUsdDay(4.99);}
					
					break;
					
				default:
					break;
				}
				break;
			case XMLStreamConstants.END_ELEMENT:
				name = reader.getLocalName().replaceAll("-", "").toUpperCase();
				
				if (FlowerTypeEnum.valueOf(name) == FlowerTypeEnum.FLOWERFORRENT) {
					return fr;
				}
				break;
			}
		}
		throw new XMLStreamException("Unknown element in tag flower-for-rent");
	}

	
	private VisualParameters getXMLVisualParameters(XMLStreamReader reader) throws XMLStreamException {
		VisualParameters visualParameters = new VisualParameters();
		int type;
		String name;
		while (reader.hasNext()) {
			type = reader.next();
			switch (type) {
			case XMLStreamConstants.START_ELEMENT:
				name = reader.getLocalName();
				switch (FlowerTypeEnum.valueOf(name.replaceAll("-", "").toUpperCase())) {
				case STEMCOLOR:
					visualParameters.setStemColor(getXMLText(reader));
					break;
				case LEAFCOLOR:
					visualParameters.setLeafColor(getXMLText(reader));
					break;
				case AVERAGESIZECM:
					visualParameters.setAverageSizeCm(Integer.parseInt(getXMLText(reader)));
					break;
				default:
					break;
				}
				break;
			case XMLStreamConstants.END_ELEMENT:
				name = reader.getLocalName();
				if (name == FlowerTypeEnum.VISUALPARAMETERS.getValue()) {
					return visualParameters;
				}
				break;
			}
		}
		throw new XMLStreamException("Unknown element in tag visual-parameters");
	}
	
	
	private GrowingTips getXMLGrowingTips(XMLStreamReader reader) throws XMLStreamException {
		GrowingTips growingTips = new GrowingTips();
		int type;
		String name;
		while (reader.hasNext()) {
			type = reader.next();
			switch (type) {
			case XMLStreamConstants.START_ELEMENT:
				name = reader.getLocalName();
				switch (FlowerTypeEnum.valueOf(name.replaceAll("-", "").toUpperCase())) {
				case TEMPERATUREC:
					growingTips.setTemperatureC(Short.parseShort(getXMLText(reader)));
					break;
				case LIGHTING:
					growingTips.setLighting(getXMLText(reader));
					break;
				case WEEKWATERINGML:
					growingTips.setWeekWateringMl(Integer.parseInt(getXMLText(reader)));
					break;
				default:
					break;
				}
				break;
			case XMLStreamConstants.END_ELEMENT:
				name = reader.getLocalName();
				if (name == FlowerTypeEnum.GROWINGTIPS.getValue()) {
					return growingTips;
				}
				break;
			}
		}
		throw new XMLStreamException("Unknown element in tag growing-tips");
	}

	
	private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
		String text = null;
		if (reader.hasNext()) {
			reader.next();
			text = reader.getText();
		}
		return text;
	}
}
