<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
<title>XML web parsing</title>
</head>
<body>
	<p>XML WEB PARSING</p>

	<form name="parsing-options" action="parse" method="POST">
		<p>
		 	<input type="radio" name="parser" value="dom" > DOM <br>
			<input type="radio" name="parser" value="sax" > SAX <br>
			<input type="radio" name="parser" value="stax" > STAX <br>
		</p>
		<input type="submit" name="button" value="PARSE"/>
	</form>

	<p> ${res} </p>
	 
</body>
</html>