package by.epam.xpw.controller;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.xpw.service.parser.XMLParser;


@WebServlet("/parse")
public class Controller extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    public Controller() {
        super();
        
    }

	public void init(ServletConfig config) throws ServletException {
		
	}

	public void destroy() {
		super.destroy();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String path = request.getServletContext().getRealPath("/");
		String parser = request.getParameter("parser");
		String temp = XMLParser.parse(parser, path);
		request.setAttribute("res", temp);
		request.getRequestDispatcher("index.jsp").forward(request, response);

	}

}
