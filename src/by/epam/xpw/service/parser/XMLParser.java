package by.epam.xpw.service.parser;

import by.epam.xpw.service.validator.Validator;
import static by.epam.xpw.entity.Constants.XML_INPUT_FILE_PATH;
import static by.epam.xpw.entity.Constants.XSD_INPUT_FILE_PATH;
import static by.epam.xpw.entity.Constants.NO_PARSER_SELECTED_ERROR_MESSAGE;
import static by.epam.xpw.entity.Constants.PARSING_ERROR_MESSAGE;

public class XMLParser {

	public static String parse(String parser, String path) {
		
		if (Validator.validate(path + XML_INPUT_FILE_PATH, path + XSD_INPUT_FILE_PATH)) {
			
			if(parser == null) {
				return NO_PARSER_SELECTED_ERROR_MESSAGE;
			}
			
			FlowerBuilderFactory sFactory = new FlowerBuilderFactory();
			AbstractFlowersBuilder builder = sFactory.createFlowerBuilder(parser);
			builder.buildListFlowers(path + XML_INPUT_FILE_PATH);
			StringBuilder temp = new StringBuilder();
			temp.append("Parsed by " + parser.toUpperCase() + " parser<br><br>");
			temp.append(builder.getFlowers());
			return temp.toString();
			
		}	else {
				return PARSING_ERROR_MESSAGE;
		}	
	}

}