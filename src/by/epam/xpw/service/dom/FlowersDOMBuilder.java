package by.epam.xpw.service.dom;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import by.epam.xpw.entity.FlowerForRent;
import by.epam.xpw.entity.FlowerForSale;
import by.epam.xpw.entity.FlowerType;
import by.epam.xpw.entity.GrowingTips;
import by.epam.xpw.entity.MultiplyingType;
import by.epam.xpw.entity.SoilType;
import by.epam.xpw.entity.VisualParameters;
import by.epam.xpw.service.parser.AbstractFlowersBuilder;

public class FlowersDOMBuilder extends AbstractFlowersBuilder{
	
	private static final Logger LOGGER = LogManager.getLogger(FlowersDOMBuilder.class);
	
	private List<FlowerType> flowers;
	private DocumentBuilder docBuilder;

	public FlowersDOMBuilder() {
		this.flowers = new ArrayList<FlowerType>();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			docBuilder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			LOGGER.error("Parser configuration error: " + e);
		}
	}
	
	public FlowersDOMBuilder(List<FlowerType> flowers) {
		super(flowers);
	}
	
	public List<FlowerType> getFlowers() {
		return flowers;
	}

	@Override
	public void buildListFlowers(String fileName) {
		Document doc = null;
		try {

			doc = docBuilder.parse(new FileInputStream(fileName));
			Element root = doc.getDocumentElement();

			NodeList flowersList = root.getElementsByTagName("flower-for-sale");
			for (int i = 0; i < flowersList.getLength(); i++) {
				Element flowerElement = (Element) flowersList.item(i);
				FlowerType flower = buildFlowerForSale(flowerElement);
				flowers.add(flower);
			}
			
			flowersList = root.getElementsByTagName("flower-for-rent");
			for (int i = 0; i < flowersList.getLength(); i++) {
				Element flowerElement = (Element) flowersList.item(i);
				FlowerType flower = buildFlowerForRent(flowerElement);
				flowers.add(flower);
			}
			
			
		} catch (IOException e) {
			LOGGER.error("File error or I/O error: " + e);
		} catch (SAXException e) {
			LOGGER.error("Parsing failure: " + e);
		}
	}
	
	private void buildFlowerType(FlowerType flower, Element flowerElement) {
		
		flower.setName(getElementTextContent(flowerElement, "name"));
		flower.setFlowerId(flowerElement.getAttribute("flower-id"));
		flower.setForSale(Boolean.parseBoolean(flowerElement.getAttribute("for-sale")));
		flower.setSoil(SoilType.fromValue(getElementTextContent(flowerElement, "soil")));
		flower.setOrigin(getElementTextContent(flowerElement, "origin"));
		
		VisualParameters visualParameters = flower.getVisualParameters();
		Element visualParametersElement = (Element) flowerElement.getElementsByTagName("visual-parameters").item(0);
		visualParameters.setStemColor(getElementTextContent(visualParametersElement, "stem-color"));
		visualParameters.setLeafColor(getElementTextContent(visualParametersElement, "leaf-color"));
		visualParameters.setAverageSizeCm(Integer.parseInt(getElementTextContent(visualParametersElement, "average-size-cm")));
		
		GrowingTips growingTips = flower.getGrowingTips();
		Element growingTipsElement = (Element) flowerElement.getElementsByTagName("growing-tips").item(0);
		growingTips.setTemperatureC(Short.parseShort(getElementTextContent(growingTipsElement, "temperature-c")));
		growingTips.setLighting(getElementTextContent(growingTipsElement, "lighting"));
		growingTips.setWeekWateringMl(Integer.parseInt(getElementTextContent(growingTipsElement, "week-watering-ml")));
		
		flower.setMultiplying(MultiplyingType.fromValue(getElementTextContent(flowerElement, "multiplying")));
	}
	
	

	private FlowerType buildFlowerForSale(Element flowerElement) {
		
		FlowerForSale flower = new FlowerForSale();

		buildFlowerType(flower, flowerElement);
				
		String temp = flowerElement.getAttribute("expiration-status");
		if(!temp.isEmpty()) {
			flower.setExpirationStatus(temp);
		} else {flower.setExpirationStatus("good");};
		
		temp = getElementTextContent(flowerElement, "price-usd");
		if(!temp.isEmpty()) {
			flower.setPriceUsd(Double.parseDouble(temp));
		} else {flower.setPriceUsd(4.99);}
		
		return flower;
	}
	
	
	private FlowerType buildFlowerForRent(Element flowerElement) {
		FlowerForRent flower = new FlowerForRent();

		buildFlowerType(flower, flowerElement);
		
		flower.setRentalTimeDays(Integer.parseInt(flowerElement.getAttribute("rental-time-days")));
		
		String temp = getElementTextContent(flowerElement, "price-usd-day");
		if(!temp.isEmpty()) {
			flower.setPriceUsdDay(Double.parseDouble(getElementTextContent(flowerElement, "price-usd-day")));
		} else {flower.setPriceUsdDay(4.99);}
	
		return flower;
	}

	
	private static String getElementTextContent(Element element, String elementName) {
		NodeList nList = element.getElementsByTagName(elementName);
		Node node = nList.item(0);
		String text = node.getTextContent();
		return text;
	}
}
