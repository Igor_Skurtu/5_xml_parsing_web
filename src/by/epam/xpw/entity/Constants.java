package by.epam.xpw.entity;

public class Constants {
	
	public static final String XML_INPUT_FILE_PATH = "data/Flowers.xml";
	public static final String XSD_INPUT_FILE_PATH = "data/Flowers.xsd";
	public static final String NO_PARSER_SELECTED_ERROR_MESSAGE = "No parser selected ! Please, select parser.";
	public static final String PARSING_ERROR_MESSAGE = "PARSING FAILED !!!";

}
