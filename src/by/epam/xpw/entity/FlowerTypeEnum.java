package by.epam.xpw.entity;

public enum FlowerTypeEnum {
	
	FLOWERS("flowers"), FLOWERID("flower-id"), FORSALE("for-sale"), EXPIRATIONSTATUS("expiration-status"), RENTALTIMEDAYS("rental-time-days"),
	FLOWER("flower"), FLOWERFORSALE("flower-for-sale"), FLOWERFORRENT("flower-for-rent"),
	NAME("name"), SOIL("soil"), ORIGIN("origin"), MULTIPLYING("multiplying"), STEMCOLOR("stem-color"), LEAFCOLOR("leaf-color"),
	AVERAGESIZECM("average-size-cm"), TEMPERATUREC("temperature-c"), LIGHTING("lighting"), WEEKWATERINGML("week-watering-ml"),
	PRICEUSD("price-usd"), PRICEUSDDAY("price-usd-day"),
	VISUALPARAMETERS("visual-parameters"), GROWINGTIPS("growing-tips");
	
	private String value;

	private FlowerTypeEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
